import { registerAs } from '@nestjs/config';

// export default () => ({
//   keyOne: process.env.TEST_ONE,
// });

export default registerAs('testing', () => ({
  // REGISTER AS IS A FACTORY FUNCTION
  //BY THIS THE PROCESS.ENV OBJECT WILL CONTAIN THE FULLY RESOLVED ENV VARS
  keyOne: process.env.TEST_ONE,
  keyTwo: process.env.TEST_TWO,
}));
