import { Injectable, Inject } from '@nestjs/common';
import { ConfigService, ConfigType } from '@nestjs/config';
import configuration from './config/configuration';

interface Testing {
  file: number;
 
}

@Injectable()
export class TestService {
  constructor(
    private configService: ConfigService,
    // private configService: ConfigService<Testing>, // if describe the type here then no need to use types below
    // WE CAN ALSO USE
    // @Inject(configuration.KEY)
    // private testConfig: ConfigType<typeof configuration>, // optional for register as
  ) {}

 

  testenv = this.configService.get<any>('testing.keyOne'); // by using registeras factory function
//   envFileOne = this.configService.get<any>('TEST_ONE');
//    envFileTwo = this.configService.get<any>('TEST_TWO');
//   envFileOne = this.configService.get<any>('configuration','optional one'); // CAN USE DEFAULT IF FIRST ONE IS UNDEFINED
envFileOne = this.configService.get<Testing>('TEST_ONE'); // can also use ts for generic types

// customOne = this.envFileOne.keyOne;

  getTest(): string {
    console.log(this.envFileOne +'env file one');
    console.log(this.testenv +'test env variable');
    // console.log(this.customOne);
    // console.log(this.envFileTwo +'env file two');
    console.log(process.env.TEST_ONE); // CHECKING WITH IS GLOBAL =TRUE
    return 'this is a test success message';
  }
}
