import { TestController } from './test.controller';
import { TestService } from './test.service';
import { Module, } from '@nestjs/common';


import { ConfigModule} from '@nestjs/config';
import configuration from './config/configuration';


@Module({
  imports: [
      // ConfigModule,
      ConfigModule.forFeature(configuration),
    ],
  controllers: [TestController],
  providers: [TestService],
})
export class FeatureModule {}
