import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ConfigModule } from '@nestjs/config';
import configuration from './config/configuration';
import { FeatureModule } from './feature.module';
import * as Joi from '@hapi/joi';

@Module({
  imports: [
    FeatureModule,
    ConfigModule.forRoot({
      // validationSchema: Joi.object({
      //   NODE_ENV: Joi.string()
      //     .valid('development', 'production')
      //     .default('development'),
      //   TEST: Joi.string(),
      // }),

      load:[configuration] // to load multiple files
      // isGlobal:true // to set module globally
      // envFilePath:'.env.test',
      // envFilePath:['.env','.env.test'],
      // ignoreEnvFile:true // this if don't wanna load dot env file 
    }),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
